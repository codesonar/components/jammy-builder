# Jammy Builder

This is a basic Ubuntu Jammy builder container that can be used in pipelines. It uses the [CI Component publishing](https://gitlab.com/guided-explorations/ci-components/ci-component-pub) component to create the Docker container.